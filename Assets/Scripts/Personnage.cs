using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Personnage : MonoBehaviour
{
    [field: SerializeField] public float healthPoints { get; protected set; } = 100;
    [field: SerializeField] public float attackPoints { get; protected set; } = 10;
    [field: SerializeField] public float moveSpeed { get; protected set; } = 2.5f;
    
    public int id = 0;

    protected Personnage(float hp = 100f, float ap = 10f, float speed = 2.5f, int id = 0)
    {
        this.healthPoints = hp;
        this.attackPoints = ap;
        this.moveSpeed = speed;
        this.id = id;
    }

    public void Update()
    {
        if (healthPoints <= 0)
        {
            Destroy(gameObject);
        }
    }

    public abstract void Speak();

    public virtual void Move(Transform tr)
    {
        tr.position += tr.forward * moveSpeed * Time.deltaTime;
    }

    public virtual void ShowStats()
    {
        Debug.Log($"Joueur - HP: {healthPoints}\n Joueur AP: {attackPoints}\n Joueur - ID: {id}");
    }

    public virtual void Attack(Personnage perso)
    {
        Debug.Log($"{this.name} attacking {perso.name}!");
        perso.TakeDamage(attackPoints);
    }

    public virtual void TakeDamage(float damage)
    {
        healthPoints = Mathf.Max(0, healthPoints - attackPoints);
    }
}
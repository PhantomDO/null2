using UnityEngine;

public class Pomme : Fruit
{
    public string forme { get; private set; }

    public Pomme()
    {
        nom = "pomme";
        couleur = "rouge";
        forme = "trapue/sph�rique";
    }

    public Pomme(string color)
    {
        nom = "pomme";
        couleur = color;
        forme = "trapue/sph�rique";
    }

    public override void Couper()
    {
        Debug.Log($"On coupe un(e) {nom} de couleur {couleur} et de forme {forme}.");
    }
}
using UnityEngine;

public class Fruit
{
    public string nom { get; set; }
    public string couleur { get; protected set; }

    public Fruit()
    {
        couleur = "orange";
        Debug.Log("Fruit - Constructeur par d�faut");
    }

    public Fruit(string color)
    {
        couleur = color;
        Debug.Log("Fruit - Constructeur alternatif");
    }

    public virtual void Couper()
    {
        Debug.Log($"On coupe un fruit: {couleur}");
    }
}
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class POO : MonoBehaviour
{
    private Pomme pomme;
    private Fruit fruitViolet;

    // Start is called before the first frame update
    void Start()
    {
        fruitViolet = new Fruit("violet");
        pomme = new Pomme();

        fruitViolet.nom = "raisins";
        fruitViolet.Couper();
        pomme.Couper();
    }

}

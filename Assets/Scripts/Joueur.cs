using UnityEngine;

public class Joueur : Personnage
{
    public Joueur(float hp = 100, float ap = 10, int id = 0) : base(hp, ap, id)
    {
    }

    public override void Speak()
    {
        Debug.Log("Salut, sir!");
    }

    public override void Attack(Personnage perso)
    {
        healthPoints += 10;
        base.Attack(perso);
    }

    public override void Move(Transform tr)
    {
        RaycastHit hit;
        Vector3 origin = transform.position;
        Vector3 rayDir = -transform.up + transform.forward;
        if (Physics.Raycast(origin, rayDir, out hit, Mathf.Infinity))
        {
            Debug.DrawRay(origin, rayDir * hit.distance, Color.yellow);
        }

        if (hit.transform == null || !hit.transform.CompareTag("Terrain"))
        {
            Vector3 dir = new Vector3(Random.Range(-1.0f, 1.0f), 0, -rayDir.z);
            transform.rotation = Quaternion.LookRotation(dir.normalized, Vector3.up);
        }
        
        base.Move(transform);
    }
}
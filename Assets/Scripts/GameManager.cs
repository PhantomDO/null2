using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public float fireRate = 1.0f;
    public GameObject[] personnages;

    private Joueur _controleJoueur;
    private Ennemi _controleEnnemi;

    private bool _firstEncounter = false;
    private bool _hasBeenDamaged = false;
    private float _timeSinceLastAttack = 0.0f;

    // Start is called before the first frame update
    void Awake()
    {
        foreach (var go in personnages)
        {
            if (go.TryGetComponent(out Joueur joueur))
            {
                _controleJoueur = joueur;
            }

            if (go.TryGetComponent(out Ennemi ennemi))
            {
                _controleEnnemi = ennemi;
            }
        }

        _firstEncounter = true;
        _hasBeenDamaged = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (_controleEnnemi == null || _controleJoueur == null)
        {
#if !UNITY_EDITOR
            Application.Quit();
#else
            UnityEditor.EditorApplication.isPlaying = false;
#endif
        }
        else
        {

            if (_hasBeenDamaged && Time.time >= _timeSinceLastAttack + fireRate)
            {
                _hasBeenDamaged = false;
            }

            if (!_hasBeenDamaged && Vector3.Distance(_controleEnnemi.transform.position, _controleJoueur.transform.position) <= 1.0f)
            {
                if (_firstEncounter)
                {
                    _controleEnnemi?.Speak();
                    _controleJoueur?.Speak();
                    _firstEncounter = false;
                }
                else
                {
                    var rdm = Random.Range(0, 2);
                    if (rdm == 0) _controleJoueur?.Attack(_controleEnnemi);
                    else _controleEnnemi?.Attack(_controleJoueur);

                    _hasBeenDamaged = true;
                    _timeSinceLastAttack = Time.time;
                }
            }
            else
            {
                _controleEnnemi?.Move(_hasBeenDamaged ? null : _controleJoueur.transform);
                _controleJoueur?.Move(null);
            }

        }
    }
}

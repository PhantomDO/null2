using UnityEngine;

public class Ennemi : Personnage
{
    private bool _firstTimeNull = false;
    private Vector3 _nullDirection = Vector3.zero;

    public Ennemi(float hp = 100, float ap = 10, int id = 0) : base(hp, ap, id)
    {
    }

    public override void Speak()
    {
        Debug.Log("En garde!");
    }

    public override void Attack(Personnage perso)
    {
        moveSpeed *= 1.1f;
        base.Attack(perso);
    }

    public override void Move(Transform tr)
    {
        if (tr != null && _firstTimeNull)
        {
            _firstTimeNull = false;
            _nullDirection = Vector3.zero;
        }
        else if (!_firstTimeNull && tr == null)
        {
            _firstTimeNull = true;
            _nullDirection = new Vector3(Random.Range(-1.0f, 1.0f), 0, -transform.forward.z);
        }


        Vector3 dir = tr!= null ? tr.position - transform.position : _nullDirection;
        transform.rotation = Quaternion.LookRotation(dir.normalized, Vector3.up);
        base.Move(transform);
    }
}